const {getAPI} = require('./socket-client')

let api = getAPI('http://localhost:4000', {strategy: 'custom', username: 'alex'})

api.events.on('logged', () => {
  api.service('session-manager').create({action: "join", channel: 'team'})
})
