const feathers = require('@feathersjs/feathers')
const socketio = require('@feathersjs/socketio-client')
const auth = require('@feathersjs/authentication-client')
const io = require('socket.io-client')
const events = require('events')
const eventEmitter = new events.EventEmitter()

// https://socket.io/docs/client-api/
function getAPI(url, cred) {

  let api = feathers()
  api.events = eventEmitter

  let socket = io(url, {
    transports: ['websocket'],
    path: '/socket.io'
  })

  api.configure(socketio(socket))
  api.configure(auth())

  socket.on('disconnect', () => {
    console.info('Disconnected')
  })

  socket.on('reconnecting', () => {
    console.info('Reconnecting...')
  })

  socket.on('reconnect', () => {
    console.info('Reconnected')
  })

  socket.on('connect', () => {
    console.info('Connected')
    api.authenticate(cred).then(() => {
        eventEmitter.emit('logged')
    }).catch(e => {
      console.error(`Fail to authenticate.`, e.name, e.message, e.errors)
    })
  })

  api.service('session-manager').on('created', data => console.info(data))

  return api
}

module.exports = {
  getAPI
}


