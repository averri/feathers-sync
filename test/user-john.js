const {getAPI} = require('./socket-client')

let api = getAPI('http://localhost:4000', {strategy: 'custom', username: 'john'})

api.events.on('logged', () => {
  api.service('session-manager').create({action: "join", channel: 'alone'})
})
