const socketio = require('@feathersjs/socketio')
const sync = require('feathers-sync')

module.exports = app => {

  app.configure(socketio())

  app.configure(sync({
    uri: 'redis://localhost:6379'
  }))
}
