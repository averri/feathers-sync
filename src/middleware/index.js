const socketio = require('./socketio')

module.exports = app => {
  app.configure(socketio)
}
