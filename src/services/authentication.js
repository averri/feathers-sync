module.exports = app => {
  app.service('authentication').hooks({
    after: {
      create: ({result}) => {
        result.joinChannel = null
        result.leaveChannel = null
      }
    }
  })
}
