const {NotFound} = require('@feathersjs/errors')

module.exports = app => {

  const users = {
    "alex": {id: 1, username: 'alex', channel: 'team'},
    "sara": {id: 2, username: 'sara', channel: 'team'},
    "john": {id: 3, username: 'john', channel: 'alone'}
  }

  const getUser = username => {
    let user = users[username]
    if (user) {
      return user
    } else {
      throw new NotFound('User not found.')
    }
  }

  app.use('users', {

    id: 'username',

    async setup(app, path) {
      this.app = app
      this.path = path
    },

    async find(params) {
      return getUser(params.query.username || '?')
    },

    async get(id, params) {
      return getUser(id)
    }

  })

}
