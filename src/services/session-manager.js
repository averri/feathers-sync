const {authenticate} = require('@feathersjs/authentication').hooks

const associateUser = ctx => {
  ctx.data.username = ctx.params.user.username
}

module.exports = app => {

  app.use('session-manager', {
    setup(app) {
      this.app = app
    },

    async create(data) {
      return data
    }
  })

  app.service('session-manager').hooks({
    before: {
      all: [
        authenticate('jwt'),
        associateUser
      ]
    }
  })

  // Join websocket clients to the requested channel.
  app.service('session-manager').on('created', (msg, ctx) => {
    let {connection} = ctx.params
    if (connection) {
      console.debug('on created', msg)
      switch (msg.action) {
        case 'join':
          app.get('connection-manager')(connection).join(msg.channel)
          break
        case 'leave':
          app.get('connection-manager')(connection).leave(msg.channel)
          break
      }
    }
  })

  app.service('session-manager').publish('created', ({channel}) => {
    return app.channel(channel)
  })

}
