module.exports = app => {
  app.configure(require('./session-manager'))
  app.configure(require('./users'))
  app.configure(require('./authentication'))
}
