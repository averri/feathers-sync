const {AuthenticationService, JWTStrategy} = require('@feathersjs/authentication')
const CustomStrategy = require('./CustomStrategy')

module.exports = app => {
  const authentication = new AuthenticationService(app)
  authentication.register('jwt', new JWTStrategy())
  authentication.register('custom', new CustomStrategy())
  app.use('/authentication', authentication)
};
