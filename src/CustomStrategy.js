const {AuthenticationBaseStrategy} = require('@feathersjs/authentication')

class CustomStrategy extends AuthenticationBaseStrategy {
  async authenticate(authentication, params) {
    console.debug(`authenticating`, authentication)
    let {username} = authentication
    let user = await this.app.service('users').get(username)
    return {user, authenticated: true}
  }
}

module.exports = CustomStrategy
