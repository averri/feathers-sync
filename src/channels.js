module.exports = function (app) {
  if (typeof app.channel !== 'function') {
    // If no real-time functionality has been configured just return
    return
  }

  const connectionId = connection => {
    let {headers} = connection
    return headers['sec-websocket-key']
  }

  const getConnections = () => {
    let connections = app.get('connections')
    if (connections === null || connections === undefined) {
      app.set('connections', {})
    }
    return app.get('connections')
  }

  const storeConnection = connection => {
    let connections = getConnections()
    let id = connectionId(connection)
    if (id) {
      connections[id] = connection
      app.set('connections', connections)
    }
  }

  const loadConnection = connection => {
    let id = connectionId(connection)
    if (id) {
      return getConnections()[id]
    }
    return null
  }

  app.set('connection-manager', connection => {
    let conn = loadConnection(connection)
    return {
      join: name => {
        if (conn) app.channel(name).join(conn)
      },
      leave: name => {
        if (conn) app.channel(name).leave(conn)
      }
    }
  })

  app.on('connection', connection => {
    storeConnection(connection)
  })

  app.on('disconnect', connection => {
    let id = connectionId(connection)
    if (id) delete app.get('connections')[id]
    let {user} = connection
    console.debug(`User disconnected: ${user.username}`)
  })

  app.on('login', (authResult, {connection}) => {
    if (connection) {
      app.channel('authenticated').join(connection)
    }
  })

}
